package com.mle.musicpimp.beam

/**
 *
 * @author mle
 */
case class BeamCommand(track: String,
                       uri: String,
                       username: String,
                       password: String)